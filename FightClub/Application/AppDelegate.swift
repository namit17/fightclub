//
//  AppDelegate.swift
//  FightClub
//
//  Created by Namit Agrawal on 13/07/21.
//

import UIKit
import Kingfisher

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        configure()
        if #available(iOS 13.0, *) {
            // UI created in scene delegate
        } else {
            let window = UIWindow(frame: UIScreen.main.bounds)
            self.window = window
            setDashboardRoot()
        }
        return true
    }
    
    // MARK: UISceneSession Lifecycle
    
    @available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }
    
    @available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    
}

extension AppDelegate {
    private func configure() {
        self.configureKingfisher()
    }
    
    private func configureKingfisher() {
        let imageCache = ImageCache.default
        
        // Limit memory cache size to 300 MB.
        imageCache.memoryStorage.config.totalCostLimit = 300 * 1024 * 1024
        
        // Memory image expires after 30 days.
        imageCache.memoryStorage.config.expiration = .seconds(60 * 60 * 24 * 30)
        
        // Remove only expired.
        imageCache.cleanExpiredMemoryCache()
    }
}

// MARK: - Routing Methods

extension AppDelegate {
    
    func setRoot(viewController: UIViewController) {
        window?.rootViewController = nil
        window?.rootViewController = viewController
        window?.makeKeyAndVisible()
        UIView.transition(with: Constant.Application.delegate.window!, duration: 0.3, options: .transitionCrossDissolve, animations: nil, completion: nil)
    }
    
    func setDashboardRoot() {
        let loginVC = Storyboard.main.instantiate(vcType: MovieListVC.self)
        let navigationController = UINavigationController(rootViewController: loginVC)
        navigationController.navigationBar.isHidden = true
        setRoot(viewController: navigationController)
    }
}
