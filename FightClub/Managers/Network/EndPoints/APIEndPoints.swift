//
//  ReactVendorEndPoints.swift
//

import Foundation

// MARK: - EndPoint Configuration

public enum HTTPRestApi {
    
    //ReactVendor
    case movies(parameter: Parameters)
    case movieDetail(_ id: String, parameter: Parameters)
    case movieReviews(_ id: String, parameter: Parameters)
    case movieCredits(_ id: String, parameter: Parameters)
    case movieSimilar(_ id: String, parameter: Parameters)
}

// MARK: Parameter Declaration
extension HTTPRestApi: HTTPRequestConfiguration {
    
    var baseUrl: URL {
        guard let devURL = URL(string: "https://api.themoviedb.org/3/") else {
            fatalError("Development URL not configured")
        }
        return devURL
    }
    
    var path: String {
        switch self {
        case .movies:
            return "movie/now_playing"
            
        case .movieDetail(let id, _):
            return "movie/" + id

        case .movieReviews(let id, _):
            return "movie/" + id + "/reviews"

        case .movieCredits(let id, _):
            return "movie/" + id + "/credits"

        case .movieSimilar(let id, _):
            return "movie/" + id + "/similar"
        }
    }
    
    var method: HTTPMethods {
        switch self {
        case .movies, .movieDetail, .movieReviews, .movieCredits, .movieSimilar:
            return .get
        }
    }
    
    var task: HTTPTask {
        switch self {
        case .movies(let urlParameters),
             .movieDetail(_, let urlParameters),
             .movieReviews(_, let urlParameters),
             .movieCredits(_, let urlParameters),
             .movieSimilar(_, let urlParameters):
            return .requestParameters(urlParameters: urlParameters, encoding: .url)
            
        default:
            return .request
        }
    }
    
    var headers: HTTPHeaders? {
        return [:]
    }
    
    var authorizationPolicy: HTTPAuthorizationPolicy {
        return .signedIn
    }
}
