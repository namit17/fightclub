//
//  Response.swift
//

import Foundation

struct Response: Decodable {
    let success: Bool
    let message: String?
    let error: Message?
    
    struct Message: Decodable {
        let message: String
    }
}
