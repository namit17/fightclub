//
//  HTTPResponse.swift
//

import Foundation

// MARK: - Enum of NetworkResponse Type

public enum NetworkError: Error {
    case authenticationError
    case badRequest
    case outDated
    case noData
    case unableToDecode
    case noInternetConnection
    case apiFailed
    case forceUpdate
    case other(message: String)
}

extension NetworkError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .authenticationError:
            return NSLocalizedString("AUTHENTICATION_ERROR", comment: "Localized value not found for key: AUTHENTICATION_ERROR")
            
        case .badRequest:
            return NSLocalizedString("BAD_REQUEST_ERROR", comment: "Localized value not found for key: BAD_REQUEST_ERROR")
            
        case .outDated:
            return NSLocalizedString("URL_OUTDATED_ERROR", comment: "Localized value not found for key: URL_OUTDATED_ERROR")
            
        case .noData:
            return NSLocalizedString("NO_DATA_ERROR", comment: "Localized value not found for key: NO_DATA_ERROR")
            
        case .unableToDecode:
            return NSLocalizedString("UNABLE_TO_DECODE_ERROR", comment: "Localized value not found for key: UNABLE_TO_DECODE_ERROR")
            
        case .noInternetConnection:
            return NSLocalizedString("CHECK_NETWORK_CONNECTION", comment: "Localized value not found for key: CHECK_NETWORK_CONNECTION")
            
        case .apiFailed:
            return NSLocalizedString("API_FAILED", comment: "Localized value not found for key: API_FAILED")
            
        case .forceUpdate:
            return NSLocalizedString("FORCE_UPDATE", comment: "Localized value not found for key: FORCE_UPDATE")
            
        case let .other(message):
            return message
        }
    }
}

// MARK: - Handle Response

struct HTTPResponse {
    static func handleHTTPResponse<Model: Decodable>(model: Model.Type, endPoint: HTTPRestApi, data: Data?, response: URLResponse?, error: Error?, completion: @escaping (Result<Model, Error>) -> Void) {
        guard error == nil else {
            print("error -- \(String(describing: error))")
            if (error! as NSError).code == -1009 {
                completion(Result.failure(NetworkError.noInternetConnection))
            } else {
                completion(Result.failure(error!))
            }
            return
        }
        
        guard let response = response as? HTTPURLResponse else {
            completion(Result.failure(NetworkError.apiFailed))
            return
        }
        
        print("==================================")
        print("API baseUrl: \(endPoint.baseUrl)\nAPI path: \(endPoint.path)")
        print("Response: \(data?.prettyPrintedJSONString ?? "")")
        print("==================================")
        
        let result = self.handleNetworkResponse(response)
        switch result {
        case .success:
            guard let data = data else {
                completion(Result.failure(NetworkError.noData))
                return
            }
            
            do {
//                let json = try JSONSerialization.jsonObject(with: data, options: []) as? Parameters
//                if let jsonData = json {
//                    guard jsonData["success"] as? Bool == true else {
//                        Response.parseJSON(data, completion: { (responseModel, error) in
//                            completion(Result.failure(NetworkError.other(message: responseModel?.error?.message ?? NetworkError.apiFailed.errorDescription!)))
//                        })
//                        return
//                    }
//                }
                
                model.parseJSON(data, completion: { (parsedModel, error) in
                    guard let parsedModel = parsedModel else {
                        guard let err = error else { return }
                        print("parsing error -- \(err )")
                        completion(Result.failure(NetworkError.unableToDecode))
                        return
                    }
                    completion(Result.success(parsedModel))
                })
            } catch {
                print("error in catch block \(error.localizedDescription)")
            }
            
        case .failure(let error):
            print("error -- \(error)")
            completion(Result.failure(error))
        }
    }
    
    fileprivate static func handleNetworkResponse(_ response: HTTPURLResponse) -> Result<Bool, Error> {
        switch response.statusCode {
        case 200...299:
            return Result.success(true)
            
        case 401:
            URLSession.shared.getAllTasks { (allTasks) in
                allTasks.forEach { $0.cancel() }
            }
            return Result.failure(NetworkError.authenticationError)
            
        case 400, 422:
            return Result.success(true)
            
        case 402...425:
            return Result.failure(NetworkError.apiFailed)
            
        case 426:
            return Result.failure(NetworkError.forceUpdate)

        case 427...500:
            return Result.failure(NetworkError.apiFailed)

        case 501...599:
            return Result.failure(NetworkError.badRequest)
            
        case 600:
            return Result.failure(NetworkError.outDated)
            
        default:
            return Result.failure(NetworkError.apiFailed)
        }
    }
}

extension Data {
    var prettyPrintedJSONString: String { /// NSString gives us a nice sanitized debugDescription
        guard let object = try? JSONSerialization.jsonObject(with: self, options: []),
              let data = try? JSONSerialization.data(withJSONObject: object, options: [.prettyPrinted]),
              let prettyPrintedString = NSString(data: data, encoding: String.Encoding.utf8.rawValue) else { return "" }
        
        return prettyPrintedString as String
    }
}
