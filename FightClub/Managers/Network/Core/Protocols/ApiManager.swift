//
//  ApiManager.swift
//

import Foundation

// MARK: - Protocol Declaration Manager Class
protocol ApiManager {
    func requestWithParameter<Model: Decodable>(model: Model.Type, endPoint: HTTPRestApi, completion: @escaping (Result<Model, Error>) -> Void)
    func requestWithMultipartData<Model: Decodable>(model: Model.Type, endPoint: HTTPRestApi, completion: @escaping (Result<Model, Error>) -> Void)
}
