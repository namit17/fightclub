//
//

import Foundation

protocol HTTPRequestConfiguration {
    var baseUrl: URL { get }
    var path: String { get }
    var method: HTTPMethods { get }
    var task: HTTPTask { get }
    var headers: HTTPHeaders? { get }
    var authorizationPolicy: HTTPAuthorizationPolicy { get }
}
