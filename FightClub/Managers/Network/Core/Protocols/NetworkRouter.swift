//
//  NetworkRouter.swift
// 

import Foundation

protocol NetworkRouter {
    associatedtype EndPoint: HTTPRequestConfiguration
    func request(_ route: EndPoint, completion: @escaping NetworkRouterCompletion)
    func requestFormData(_ route: EndPoint, completion: @escaping NetworkRouterCompletion)
    func cancle()
}
