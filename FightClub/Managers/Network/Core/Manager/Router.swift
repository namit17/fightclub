//
//  Router.swift
// 


import UIKit

class Router<EndPoint: HTTPRequestConfiguration>: NetworkRouter {
    
    private var task: URLSessionTask?
    
    private lazy var urlSession: URLSession = {
        return URLSession(configuration: URLSessionConfiguration.default, delegate: nil, delegateQueue: OperationQueue.main)
    }()
    
    // MARK: - HTTPTask Methods
    
    func request(_ route: EndPoint, completion: @escaping NetworkRouterCompletion) {
        let session = urlSession
        do {
            let request = try self.buildRequest(from: route)
            task = session.dataTask(with: request, completionHandler: { (data, response, error) in
                completion(data, response, error)
            })
        }catch {
            completion(nil, nil, error)
        }
        self.task?.resume()
    }
    
    func requestFormData(_ route: EndPoint, completion: @escaping NetworkRouterCompletion) {
        let session = urlSession
        do {
            let request = try self.buildRequest(from: route)
            guard let data = request.httpBody else { return }
            task = session.uploadTask(with: request, from: data) { (data, response, error) in
                completion(data, response, error)
            }
            
        } catch {
            completion(nil , nil, error)
        }
        self.task?.resume()
    }
    
    func cancle() {
        self.task?.cancel()
    }
    
    // MARK: - Build URLRequest
    
    fileprivate func buildRequest(from route: EndPoint)throws -> URLRequest {
        var request = URLRequest(url: route.baseUrl.appendingPathComponent(route.path), cachePolicy: .reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 60.0)
        request.httpMethod = route.method.rawValue
        
        if route.authorizationPolicy == .signedIn {
            self.addAdditionalHeaders(route.headers, request: &request)
        }
        
        let boundary = generateBoundry()
        
        do {
            try self.encode(&request, route: route, boundary: boundary)
            self.appendHeaders(&request, route: route, boundary: boundary)
            return request
        } catch {
            throw error
        }
    }
}


// MARK: - Parameter Encoding
extension Router {
    
    private func encode(_ request: inout URLRequest, route: EndPoint, boundary: String = "") throws {
        do {
            switch route.task {
            case .request: break
                
            case .requestParameters(let parameters, let urlParameters, let bodyEncoding):
                switch bodyEncoding {
                case .json:
                    try ParameterEncoding.encode(.json(parameters: parameters ?? [:]), urlRequest: &request)
                    
                case .url:
                    try ParameterEncoding.encode(.url(parameters: urlParameters ?? [:]), urlRequest: &request)
                    
                case .urlAndJson:
                    try ParameterEncoding.encode(.urlAndJson(parameters: parameters ?? [:], urlParameters: urlParameters ?? [:]), urlRequest: &request)
                    
                default:
                    throw ParameterEncodingError.InvalidType
                }
                
            case .requestParametersAndHeaders(let parameters, let urlParameters, let bodyEncoding, let additionalHeaders):
                self.addAdditionalHeaders(additionalHeaders, request: &request)
                switch bodyEncoding {
                case .json:
                    try ParameterEncoding.encode(.json(parameters: parameters ?? [:]), urlRequest: &request)
                    
                case .url:
                    try ParameterEncoding.encode(.url(parameters: urlParameters ?? [:]), urlRequest: &request)
                    
                case .urlAndJson:
                    try ParameterEncoding.encode(.urlAndJson(parameters: parameters ?? [:], urlParameters: urlParameters ?? [:]), urlRequest: &request)
                    
                default:
                    throw ParameterEncodingError.InvalidType
                }
                
            case .requestParametersWithImages(let parameters, let images, let files, let bodyEncoding):
                switch bodyEncoding {
                case .formData:
                    try ParameterEncoding.encode(.formData(parameters: parameters ?? [:], images: images ?? [:], files: files, boundary: boundary), urlRequest: &request)
                    
                default:
                    throw ParameterEncodingError.InvalidType
                }
            }
        } catch {
            throw error
        }
    }
}

// MARK: - HTTP Headers and Authorization
extension Router {
    private func appendHeaders(_ request: inout URLRequest, route: EndPoint, boundary: String = "") {
        var isFormData: Bool = false
        switch route.task {
        case .requestParametersWithImages(_, _, _, _):
            isFormData = true
            
        default:
            isFormData = false
        }
        
        let contentType = isFormData ? "multipart/form-data; boundary=\(boundary)" : "application/json; charset=utf-8"
        request.setValue(contentType, forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("keep-alive", forHTTPHeaderField: "Connection")
        request.setValue(Constant.Application.type, forHTTPHeaderField: "device-type")
        request.setValue(Bundle.main.releaseVersionNumber, forHTTPHeaderField: "app-version")
    }
    
    fileprivate func addAdditionalHeaders(_ additionalHeaders: HTTPHeaders?, request: inout URLRequest) {
        guard let headers = additionalHeaders else { return }
        
        for (key, value) in headers {
            print("==================================")
            print("\(key) = \(value)")
            print("==================================")
            
            request.setValue(value, forHTTPHeaderField: key)
        }
    }
    
    fileprivate func generateBoundry() -> String {
        return "Boundary-\(UUID().uuidString)"
    }
}
