//
//  NetworkManager.swift
// 

import Foundation

var Manager: ApiManager  {
    return NetworkManager.shared
}

struct NetworkManager: ApiManager {
    
    static let shared = NetworkManager()
    private let router = Router<HTTPRestApi>()
    
    // MARK: - Generic Api Call
    /* This method is for call post api, it will return model class object, success message or erorr message and true/false. */
    func requestWithParameter<Model: Decodable>(model: Model.Type, endPoint: HTTPRestApi, completion: @escaping (Result<Model, Error>) -> Void) {
        router.request(endPoint) { (data, response, error) in
            HTTPResponse.handleHTTPResponse(model: model, endPoint: endPoint, data: data, response: response, error: error, completion: completion)
        }
    }
    
    // THIS METHOD IS USED FOR UPLOAD MUILTIPART DATA, (LIKE: IMAGES, DOCUMENTS, AUDIO AND VIDEO)
    
    func requestWithMultipartData<Model: Decodable>(model: Model.Type, endPoint: HTTPRestApi, completion: @escaping (Result<Model, Error>) -> Void) {
        router.requestFormData(endPoint) { data, response, error in
            HTTPResponse.handleHTTPResponse(model: model, endPoint: endPoint, data: data, response: response, error: error, completion: completion)
        }
    }
}


