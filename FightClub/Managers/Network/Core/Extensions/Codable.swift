//
//  Codable.swift
//

import Foundation

extension Decodable {
    
    //To parse JSON data
    static func parseJSON(_ data: Data, completion: (Self?, Error?) -> ()) {
        do {
            let jsonDecoder = JSONDecoder()
            jsonDecoder.keyDecodingStrategy = .convertFromSnakeCase            
            let parsedData = try jsonDecoder.decode(self, from: data)
            completion(parsedData, nil)
        } catch {
            print("json error = \(error)")
            completion(nil, error)
        }
    }
}

