//
//  HTTPTask.swift
// 

import UIKit

// MARK: - HTTPTask Methods
public enum HTTPTask {
    case request
    case requestParameters(bodyParameters: Parameters? = nil, urlParameters: Parameters? = nil, encoding: ParameterEncodingType)
    case requestParametersAndHeaders(bodyParameters: Parameters? = nil, urlParameters: Parameters? = nil, encoding: ParameterEncodingType, additionHeaders: HTTPHeaders?)
    case requestParametersWithImages(bodyParameters: Parameters? = nil, images: MultipartImageParameters? = [:], urls: MultipartURLParameters = [:], encoding: ParameterEncodingType = .formData)
}

