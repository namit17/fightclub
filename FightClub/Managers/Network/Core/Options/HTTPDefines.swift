//
//  HTTPDefines.swift
//

import Foundation
import UIKit

// MARK: Global Parameters
public typealias Parameters = [String: Any]
public typealias MultipartImageParameters = [String: [UIImage]]
public typealias MultipartURLParameters = [String: [URL]]

// Headers
public typealias HTTPHeaders = [String: String]

public typealias NetworkRouterCompletion = (_ data: Data?, _ response: URLResponse?, _ error: Error?) -> ()
