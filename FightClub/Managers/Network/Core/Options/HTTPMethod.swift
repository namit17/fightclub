//
//  HTTPMethod.swift
//  
//

import Foundation

// MARK: HTTPS METHODS NAME DECLARATION
public enum HTTPMethods: String {
    case get = "GET"
    case post = "POST"
    case put = "PUT"
    case delete = "DELETE"
}
