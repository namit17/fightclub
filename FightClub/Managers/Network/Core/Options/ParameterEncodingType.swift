//
//  ParameterEncoding.swift
//

import Foundation

public enum ParameterEncodingType {    
    case formData
    case url
    case json
    case urlAndJson
}
