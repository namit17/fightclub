//
//  HTTPAuthorizationPolicy.swift
//

import Foundation

public enum HTTPAuthorizationPolicy {
    
    case anonymous
    
    case signedIn
    
}
