//
//  NavigationView.swift
//


import UIKit

class NavigationView: UIView {
    
  override func awakeFromNib() {
    super.awakeFromNib()
    setHeight()
    configureNavBar()
  }
  
  // MARK: - Helper Methods
  
  private func setHeight() {
    let heightCustomNavigationBar = constraints.filter { $0.firstAttribute == .height }.first
    guard let heightConstraint = heightCustomNavigationBar else { return }
    let height = NavigationView.getNavigationBarHeight()
    heightConstraint.constant = height
  }
  
  static func getNavigationBarHeight() -> CGFloat {
    let window = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
    var totalNavBarHeight: CGFloat = 0
    let navigationBarHeight: CGFloat = 44 // navigationController.navigationBar.bounds.height
    if #available(iOS 13.0, *) {
      let statusBarHeight = window?.windowScene?.statusBarManager?.statusBarFrame.height ?? 0
      totalNavBarHeight = statusBarHeight + navigationBarHeight
    } else {
      // Fallback on earlier versions
      let statusBarHeight = UIApplication.shared.statusBarFrame.height
      totalNavBarHeight = statusBarHeight + navigationBarHeight
    }
    return totalNavBarHeight
  }
  
  private func configureNavBar() {
    self.layer.masksToBounds = false
    layer.shadowOffset = CGSize(width: 0, height: 2)
    layer.shadowRadius = 3
    layer.shadowColor = UIColor.black.withAlphaComponent(0.5).cgColor
    layer.shadowOpacity = 0.4
  }
}
