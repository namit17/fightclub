//
//  Constant.swift
//

import UIKit

enum Constant {
    
    enum Screen {
        static let bounds = UIScreen.main.bounds
        static let width = bounds.width
        static let height = bounds.height
    }
    
    enum Application {
        static let name = "Fight Club"
        static let type = "ios"
        static let delegate = UIApplication.shared.delegate as! AppDelegate
        static let userDefaults = UserDefaults.standard
        static let storeURL = URL(string: "https://apps.apple.com/us/app/react-tech/id1552195213")!
        static let imageUrl = "https://image.tmdb.org/t/p/w500"
    }
}

enum ResponseType {
    case success, failure, noConnection, authenticationError, forceUpdate
}
