//
//  UIButton+Extension.swift
//
//

import UIKit

extension UIButton {
  ///
  func pulsate() {
    let scaleTransform = CGAffineTransform(scaleX: 0.8, y: 0.8)
    transform = scaleTransform
    UIView.animate(withDuration: 0.4, delay: 0, usingSpringWithDamping: 0.6, initialSpringVelocity: 0.6, options: .curveEaseInOut, animations: {
      self.transform = .identity
    })
  }
  
  func drawBorder(edges: [UIRectEdge], borderWidth: CGFloat, color: UIColor, margin: CGFloat) {
    for item in edges {
      let borderLayer: CALayer = CALayer()
      borderLayer.borderColor = color.cgColor
      borderLayer.borderWidth = borderWidth
      switch item {
      case .top:
        borderLayer.frame = CGRect(x: margin, y: 0, width: frame.width - (margin*2), height: borderWidth)
      case .left:
        borderLayer.frame =  CGRect(x: 0, y: margin, width: borderWidth, height: frame.height - (margin*2))
      case .bottom:
        borderLayer.frame = CGRect(x: margin, y: frame.height - borderWidth, width: frame.width - (margin*2), height: borderWidth)
      case .right:
        borderLayer.frame = CGRect(x: frame.width - borderWidth, y: margin, width: borderWidth, height: frame.height - (margin*2))
      case .all:
        drawBorder(edges: [.top, .left, .bottom, .right], borderWidth: borderWidth, color: color, margin: margin)
      default:
        break
      }
      self.layer.addSublayer(borderLayer)
    }
  }
  
}
