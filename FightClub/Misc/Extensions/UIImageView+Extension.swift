//
//  UIImageView+Extension.swift
// 
//

import Foundation
import Kingfisher

extension UIImageView {
  
  func setImage(_ imageUrl: String, placeholderImage: UIImage?, indicatorType: IndicatorType = .activity) {
    self.kf.indicatorType = indicatorType
    self.kf.setImage(
      with: URL.init(string: imageUrl.encodedURL()),
      placeholder: placeholderImage ?? nil,
      options: [
        .transition(.fade(1))
      ],
      progressBlock: nil) { result in
      
      switch result {
      case .success:
//                print("Task done for: \(value.source.url?.absoluteString ?? "")")
        break
        
      case .failure:
//                print("Job failed: \(error.localizedDescription)")
        break
      }
    }
  }
}

extension String {
  
  func encodedURL() -> (String) {
    return self.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
  }
}
