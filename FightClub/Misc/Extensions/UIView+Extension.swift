//
//  Custom+UIView.swift
//

import Foundation
import QuartzCore
import UIKit

extension UIView {
  
  @IBInspectable var cornerRadius: CGFloat {
    get {
      return layer.cornerRadius
    }
    set {
      layer.cornerRadius = newValue
      //      layer.masksToBounds = newValue > 0
    }
  }
  
  @IBInspectable var borderWidth: CGFloat {
    get {
      return layer.borderWidth
    }
    set {
      layer.borderWidth = newValue
    }
  }
  
  @IBInspectable var borderColor: UIColor? {
    get {
      return UIColor(cgColor: layer.borderColor!)
    }
    set {
      layer.borderColor = newValue?.cgColor ?? UIColor.white.cgColor
    }
  }
  
  @IBInspectable var shadowColor: UIColor? {
    get {
      if let color = layer.shadowColor {
        return UIColor(cgColor: color)
      } else {
        return nil
      }
    }
    set {
      layer.shadowColor = newValue!.cgColor
    }
  }
  
  /* The opacity of the shadow. Defaults to 0. Specifying a value outside the
   * [0,1] range will give undefined results. Animatable. */
  @IBInspectable var shadowOpacity: Float {
    get {
      return layer.shadowOpacity
    }
    set {
      layer.shadowOpacity = newValue
    }
  }
  
  /* The shadow offset. Defaults to (0, -3). Animatable. */
  @IBInspectable var shadowOffset: CGPoint {
    get {
      return CGPoint(x: layer.shadowOffset.width, y: layer.shadowOffset.height)
    }
    set {
      layer.shadowOffset = CGSize(width: newValue.x, height: newValue.y)
    }
  }
  
  /* The blur radius used to create the shadow. Defaults to 3. Animatable. */
  @IBInspectable var shadowRadius: CGFloat {
    get {
      return layer.shadowRadius
    }
    set {
      layer.shadowRadius = newValue
    }
  }
  
  func rotate(_ toValue: CGFloat, duration: CFTimeInterval = 0.2) {
    let animation = CABasicAnimation(keyPath: "transform.rotation")
    animation.toValue = toValue
    animation.duration = duration
    animation.isRemovedOnCompletion = false
    animation.fillMode = CAMediaTimingFillMode.forwards
    self.layer.add(animation, forKey: nil)
  }
  
  func applyShadow(color: UIColor = .black, cornerRadius: CGFloat = 0, opacity: Float = 0.5, offSet: CGSize = CGSize(width: 0, height: 0), radius: CGFloat = 1, shadowRect: CGRect? = nil) {
    layer.masksToBounds = false
    layer.cornerRadius  = cornerRadius
    layer.shadowColor   = color.cgColor
    layer.shadowOpacity = opacity
    layer.shadowOffset  = offSet
    layer.shadowRadius  = radius
    
    if let shadowRect = shadowRect {
      layer.shadowPath = UIBezierPath(rect: shadowRect).cgPath
    }
  }
}

extension UIView {
  func addDashedBorder(color: UIColor = .lightGray, cornerRadius: CGFloat = 10, lineWidth: CGFloat = 2) {
    
    if let sublayers = self.layer.sublayers {
      for sublayer in sublayers where sublayer.name == "dashedBorderLayer" {
        sublayer.removeFromSuperlayer()
        break
      }
    }
    
    let shapeLayer: CAShapeLayer = CAShapeLayer()
    let frameSize = self.frame.size
    let shapeRect = CGRect(x: 0, y: 0, width: frameSize.width, height: frameSize.height)
    
    shapeLayer.bounds = shapeRect
    shapeLayer.position = CGPoint(x: frameSize.width/2, y: frameSize.height/2)
    shapeLayer.fillColor = UIColor.clear.cgColor
    shapeLayer.strokeColor = color.cgColor
    shapeLayer.lineWidth = lineWidth
    shapeLayer.lineJoin = CAShapeLayerLineJoin.round
    shapeLayer.lineDashPattern = [6, 3]
    shapeLayer.path = UIBezierPath(roundedRect: shapeRect, cornerRadius: cornerRadius).cgPath
    shapeLayer.name = "dashedBorderLayer"
    
    self.layer.addSublayer(shapeLayer)
  }
  
  func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
    if #available(iOS 11.0, *) {
      clipsToBounds = true
      layer.cornerRadius = radius
      layer.maskedCorners = CACornerMask(rawValue: corners.rawValue)
    } else {
      let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
      let mask = CAShapeLayer()
      mask.path = path.cgPath
      layer.mask = mask
    }
  }
  
  func addBottomBorder(_ color: UIColor, height: CGFloat) {
    let border = UIView()
    border.backgroundColor = color
    border.translatesAutoresizingMaskIntoConstraints = false
    self.addSubview(border)
    border.addConstraint(NSLayoutConstraint(item: border,
                                            attribute: NSLayoutConstraint.Attribute.height,
                                            relatedBy: NSLayoutConstraint.Relation.equal,
                                            toItem: nil,
                                            attribute: NSLayoutConstraint.Attribute.height,
                                            multiplier: 1, constant: height))
    self.addConstraint(NSLayoutConstraint(item: border,
                                          attribute: NSLayoutConstraint.Attribute.bottom,
                                          relatedBy: NSLayoutConstraint.Relation.equal,
                                          toItem: self,
                                          attribute: NSLayoutConstraint.Attribute.bottom,
                                          multiplier: 1, constant: 0))
    self.addConstraint(NSLayoutConstraint(item: border,
                                          attribute: NSLayoutConstraint.Attribute.leading,
                                          relatedBy: NSLayoutConstraint.Relation.equal,
                                          toItem: self,
                                          attribute: NSLayoutConstraint.Attribute.leading,
                                          multiplier: 1, constant: 0))
    self.addConstraint(NSLayoutConstraint(item: border,
                                          attribute: NSLayoutConstraint.Attribute.trailing,
                                          relatedBy: NSLayoutConstraint.Relation.equal,
                                          toItem: self,
                                          attribute: NSLayoutConstraint.Attribute.trailing,
                                          multiplier: 1, constant: 0))
  }
  
}

extension UIView {
  func constraint(withIdentifier: String) -> NSLayoutConstraint? {
    return self.constraints.filter { $0.identifier == withIdentifier }.first
  }
}
