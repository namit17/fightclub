//
//  MovieTableViewCell.swift
//  Created by Namit Agrawal on 13/07/21.
//

import UIKit
import AFDateHelper

class MovieTableViewCell: UITableViewCell {
    
    static let reuseIdentifier = "MovieTableViewCell"
    
    // MARK: - IBOutlet
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var posterImageView: UIImageView!
    
    var bookButtonTapped: (() -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func configure(movie: Movies) {
        titleLabel.text = movie.title
        dateLabel.text = Date(fromString: movie.releaseDate ?? "", format: .isoDate)?.toString(format: .custom("MMM dd, yyyy"))
        let posterUrl = Constant.Application.imageUrl + (movie.posterPath ?? "")
        posterImageView.setImage(posterUrl, placeholderImage: nil)
    }
    
    @IBAction func bookButtonAction(_ sender: UIButton) {
        bookButtonTapped?()
    }
    
}
