//
//  CastCollectionViewCell.swift
//

import UIKit

class CastCollectionViewCell: UICollectionViewCell {
    
    static let reuseIdentifier = "CastCollectionViewCell"
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var positionLabel: UILabel!
    @IBOutlet weak var castImageView: UIImageView!
    
    // MARK: - Life Cycle Methods
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configure(cast: Cast) {
        castImageView.cornerRadius = 40
        nameLabel.text = cast.name
        positionLabel.text = cast.knownForDepartment
        let posterUrl = Constant.Application.imageUrl + (cast.profilePath ?? "")
        castImageView.setImage(posterUrl, placeholderImage: nil)
    }
    
    func configure(crew: Crew) {
        castImageView.cornerRadius = 40
        nameLabel.text = crew.name
        positionLabel.text = crew.knownForDepartment
        let posterUrl = Constant.Application.imageUrl + (crew.profilePath ?? "")
        castImageView.setImage(posterUrl, placeholderImage: nil)
    }
    
    func configure(movie: Movies) {
        castImageView.cornerRadius = 5
        nameLabel.text = movie.title
        positionLabel.isHidden = true
        let posterUrl = Constant.Application.imageUrl + (movie.posterPath ?? "")
        castImageView.setImage(posterUrl, placeholderImage: nil)
    }
}
