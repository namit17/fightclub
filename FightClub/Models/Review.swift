//
//  Review.swift
//

import Foundation

struct ReviewData: Decodable {
    let page: Int?
    let results: [Review]?
    let totalPages: Int?
    let totalResults: Int?
}

struct Review: Decodable {
    
    let author: String?
    let content: String?
    let createdAt: String?
    let id: String?
    let updatedAt: String?
    let url: String?
}
