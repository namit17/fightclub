//
//  SpokenLanguages.swift
//

import Foundation

struct SpokenLanguages: Decodable {
    let englishName: String?
    let name: String?
}

struct ProductionCountries: Decodable {
    let name: String?
}

struct ProductionCompanies: Decodable {
    let id: Int?
    let logoPath: String?
    let name: String?
    let originCountry: String?
}

struct Genres: Decodable {
    let id: Int?
    let name: String?
}
