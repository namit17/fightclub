//
//  Movies.swift
//

import Foundation

struct MoviesData: Decodable {
    let dates: Dates?
    let page: Int?
    let results: [Movies]?
    let totalPages: Int?
    let totalResults: Int?
}

struct Movies: Decodable {
    let adult: Bool?
    let backdropPath: String?
    let genreIds: [Int]?
    let id: Int?
    let originalLanguage: String?
    let originalTitle: String?
    let overview: String?
    let popularity: Double?
    let posterPath: String?
    let releaseDate: String?
    let title: String?
    let video: Bool?
    let voteAverage: Float?
    let voteCount: Int?
    
    let budget: Int?
    let genres: [Genres]?
    let homepage: String?
    let imdbId: String?
    let productionCompanies: [ProductionCompanies]?
    let productionCountries: [ProductionCountries]?
    let revenue: Int?
    let runtime: Int?
    let spokenLanguages: [SpokenLanguages]?
    let status: String?
    let tagline: String?
}

struct Dates: Decodable {
    let maximum: String?
    let minimum: String?
}
