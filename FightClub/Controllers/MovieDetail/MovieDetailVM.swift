//
//  MovieDetailVM.swift
//  FightClub
//
//  Created by Namit Agrawal on 13/07/21.
//

import Foundation

class MovieDetailVM {
    var movieId: Int?
    
    var currentMovie: Movies?

    var casts: [Cast] = []
    var crews: [Crew] = []
    var similarMovies: [Movies] = []
    var totalReviews: Int = 0

    // MARK: - Api Calling Method
    
    func getMovieDetail(_ completion: @escaping ((ResponseType, String) -> Void)) {
        let paramters: Parameters = ["api_key": "c2cfb1ea7574392aa22b9fedcbc037ac",
                                     "language": "en-US"]
        
        Manager.requestWithParameter(model: Movies.self, endPoint: .movieDetail(movieId?.description ?? "", parameter: paramters), completion: { result in
            switch result {
            case .success(let data):
                self.currentMovie = data
                completion(.success, "")
                
            case .failure(let error):
                var responseType: ResponseType = .failure
                if let error = error as? NetworkError {
                    switch error {
                    case .noInternetConnection:
                        responseType = .noConnection
                        
                    default:
                        responseType = .failure
                    }
                }
                completion(responseType, error.localizedDescription)
            }
        })
    }
    
    func getMovieReviews(_ completion: @escaping ((ResponseType, String) -> Void)) {
        let paramters: Parameters = ["api_key": "c2cfb1ea7574392aa22b9fedcbc037ac",
                                     "language": "en-US"]
        
        Manager.requestWithParameter(model: ReviewData.self, endPoint: .movieReviews(movieId?.description ?? "", parameter: paramters), completion: { result in
            switch result {
            case .success(let data):
                self.totalReviews = data.totalResults ?? 0
                completion(.success, "")
                
            case .failure(let error):
                var responseType: ResponseType = .failure
                if let error = error as? NetworkError {
                    switch error {
                    case .noInternetConnection:
                        responseType = .noConnection
                        
                    default:
                        responseType = .failure
                    }
                }
                completion(responseType, error.localizedDescription)
            }
        })
    }
    
    func getMovieCredits(_ completion: @escaping ((ResponseType, String) -> Void)) {
        let paramters: Parameters = ["api_key": "c2cfb1ea7574392aa22b9fedcbc037ac",
                                     "language": "en-US"]
        
        Manager.requestWithParameter(model: CastCrewData.self, endPoint: .movieCredits(movieId?.description ?? "", parameter: paramters), completion: { result in
            switch result {
            case .success(let data):
                self.casts = data.cast ?? []
                self.crews = data.crew ?? []
                completion(.success, "")
                
            case .failure(let error):
                var responseType: ResponseType = .failure
                if let error = error as? NetworkError {
                    switch error {
                    case .noInternetConnection:
                        responseType = .noConnection
                        
                    default:
                        responseType = .failure
                    }
                }
                completion(responseType, error.localizedDescription)
            }
        })
    }
    
    func getMovieSimilar(_ completion: @escaping ((ResponseType, String) -> Void)) {
        let paramters: Parameters = ["api_key": "c2cfb1ea7574392aa22b9fedcbc037ac",
                                     "language": "en-US"]
        
        Manager.requestWithParameter(model: MoviesData.self, endPoint: .movieSimilar(movieId?.description ?? "", parameter: paramters), completion: { result in
            switch result {
            case .success(let data):
                self.similarMovies = data.results ?? []
                completion(.success, "")
                
            case .failure(let error):
                var responseType: ResponseType = .failure
                if let error = error as? NetworkError {
                    switch error {
                    case .noInternetConnection:
                        responseType = .noConnection
                        
                    default:
                        responseType = .failure
                    }
                }
                completion(responseType, error.localizedDescription)
            }
        })
    }

}
