//
//  MovieDetailVC.swift
//  FightClub
//
//  Created by Namit Agrawal on 13/07/21.
//

import UIKit

class MovieDetailVC: UIViewController {
    
    // MARK: - IBOutlet
    
    @IBOutlet weak var posterImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var overviewLabel: UILabel!
    @IBOutlet weak var ratingsLabel: UILabel!

    @IBOutlet weak var castView: UIView!
    @IBOutlet weak var castCollectionView: UICollectionView!
    
    @IBOutlet weak var crewView: UIView!
    @IBOutlet weak var crewCollectionView: UICollectionView!
    
    @IBOutlet weak var similarMovieView: UIView!
    @IBOutlet weak var similarMovieCollectionView: UICollectionView!

    // MARK: - Instance Properties
    
    let viewModel = MovieDetailVM()
    
    // MARK: - UIViewController Life Cycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initailSetup()
    }
    
    // MARK: - Helper Methods
    
    func initailSetup() {
        castCollectionView.registerCell(cellType: CastCollectionViewCell.self)
        crewCollectionView.registerCell(cellType: CastCollectionViewCell.self)
        similarMovieCollectionView.registerCell(cellType: CastCollectionViewCell.self)

        self.castView.isHidden = viewModel.casts.count == 0
        self.crewView.isHidden = viewModel.crews.count == 0
        self.similarMovieView.isHidden = viewModel.similarMovies.count == 0
        self.ratingsLabel.text = self.viewModel.totalReviews.description + " Reviews"

        getMovieDetail()
        getMovieCredits()
        getMovieSimilar()
        getMovieReviews()
    }
    
    @IBAction func backButtonAction(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    // MARK: - API Calling Methods
    
    func getMovieDetail() {
        HudView.show()
        
        viewModel.getMovieDetail() { [weak self] (success, msg) in
            HudView.kill()
            
            guard let self = self else {
                return
            }
            
            switch success {
            case .success:
                self.titleLabel.text = self.viewModel.currentMovie?.title
                self.overviewLabel.text = self.viewModel.currentMovie?.overview
                let posterUrl = Constant.Application.imageUrl + (self.viewModel.currentMovie?.posterPath ?? "")
                self.posterImageView.setImage(posterUrl, placeholderImage: nil)

            default:
                Alert.showAlertWithMessage(msg, title: "ALERT")
            }
        }
    }
    
    func getMovieCredits() {
        HudView.show()
        
        viewModel.getMovieCredits() { [weak self] (success, msg) in
            HudView.kill()
            
            guard let self = self else {
                return
            }
            
            switch success {
            case .success:
                self.castView.isHidden = self.viewModel.casts.count == 0
                self.crewView.isHidden = self.viewModel.crews.count == 0
                self.castCollectionView.reloadData()
                self.crewCollectionView.reloadData()
                
            default:
                Alert.showAlertWithMessage(msg, title: "ALERT")
            }
        }
    }
    
    func getMovieSimilar() {
        HudView.show()
        
        viewModel.getMovieSimilar() { [weak self] (success, msg) in
            HudView.kill()
            
            guard let self = self else {
                return
            }
            
            switch success {
            case .success:
                self.similarMovieView.isHidden = self.viewModel.similarMovies.count == 0
                self.similarMovieCollectionView.reloadData()
                
            default:
                Alert.showAlertWithMessage(msg, title: "ALERT")
            }
        }
    }
    
    func getMovieReviews() {
        HudView.show()
        
        viewModel.getMovieReviews() { [weak self] (success, msg) in
            HudView.kill()
            
            guard let self = self else {
                return
            }
            
            switch success {
            case .success:
                self.ratingsLabel.text = self.viewModel.totalReviews.description + " Reviews"

            default:
                Alert.showAlertWithMessage(msg, title: "ALERT")
            }
        }
    }
}

// MARK: - UICollectionViewDelegate & UICollectionViewDataSource method

extension MovieDetailVC: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == castCollectionView {
            return viewModel.casts.count
        } else if collectionView == crewCollectionView {
            return viewModel.crews.count
        } else {
            return viewModel.similarMovies.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueCell(cellType: CastCollectionViewCell.self, indexPath: indexPath)
        
        if collectionView == castCollectionView {
            cell.configure(cast: viewModel.casts[indexPath.row])
        } else if collectionView == crewCollectionView {
            cell.configure(crew: viewModel.crews[indexPath.row])
        } else if collectionView == similarMovieCollectionView {
            cell.configure(movie: viewModel.similarMovies[indexPath.row])
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    }
}

// MARK: - UICollectionViewDelegateFlowLayout method

extension MovieDetailVC: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let screenWidth = Constant.Screen.width
        let cellWidth = (screenWidth - 30) / 4
        return CGSize(width: cellWidth, height: 180)
    }
}
