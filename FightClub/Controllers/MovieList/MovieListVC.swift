//
//  MovieListVC.swift
//  FightClub
//
//  Created by Namit Agrawal on 13/07/21.
//

import UIKit

class MovieListVC: UIViewController {
    
    // MARK: - IBOutlet
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var noDataLabel: UILabel!
    @IBOutlet weak var searchBar: UISearchBar!
    
    // MARK: - Instance Properties
    
    let viewModel = MovieListVM()
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
                                    #selector(getNowPlayingMovies),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor.red
        
        return refreshControl
    }()
    
    // MARK: - UIViewController Life Cycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initailSetup()
    }
    
    // MARK: - Helper Methods
    
    func initailSetup() {
        tableView.refreshControl = refreshControl
        tableView.registerCell(cellType: MovieTableViewCell.self)
        
        noDataLabel.isHidden = true
        getAllMovies(true)
    }
    
    // MARK: - API Calling Methods
    
    @objc func getNowPlayingMovies() {
        viewModel.reset()
        getAllMovies(true)
    }
    
    func getAllMovies(_ showLoader: Bool = false) {
        if showLoader {
            HudView.show()
        }
        
        viewModel.getMoviesAPI(searchText: searchBar.text ?? "") { [weak self] (success, msg) in
            if showLoader {
                HudView.kill()
            }
            guard let self = self else {
                return
            }
            self.refreshControl.endRefreshing()
            self.viewModel.isApiCalled = false
            
            switch success {
            case .success:
                if self.viewModel.filterMovies.count > 0 {
                    self.noDataLabel.isHidden = true
                } else {
                    self.noDataLabel.isHidden = false
                    self.noDataLabel.text = "No Data Found"
                }
                self.tableView.reloadDataInMain()
                
                
            case .noConnection:
                self.refreshControl.endRefreshing()
                self.viewModel.filterMovies = []
                self.tableView.reloadData()
                self.noDataLabel.text = "Slow or no internet connection.\nPlease check your internet connection"
                self.noDataLabel.isHidden = false
                
                Alert.showNetworkAlert {
                    self.getAllMovies(showLoader)
                }
                
            default:
                self.noDataLabel.isHidden = false
                self.noDataLabel.text = "No Data Found"
                Alert.showAlertWithMessage(msg, title: "ALERT")
            }
        }
    }
    
}

// MARK: - UITableViewDelegate & UITableViewDataSource Methods

extension MovieListVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.filterMovies.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: MovieTableViewCell.reuseIdentifier) as! MovieTableViewCell
        
        let movie = viewModel.filterMovies[indexPath.row]
        cell.configure(movie: movie)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let movie = viewModel.filterMovies[indexPath.row]
        let movieDetailVC = Storyboard.main.instantiate(vcType: MovieDetailVC.self)
        movieDetailVC.viewModel.movieId = movie.id
        navigationController?.pushViewController(movieDetailVC, animated: true)
    }
}

//MARK: - UIScrollView Delegate and DataSource

extension MovieListVC: UIScrollViewDelegate {
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let endScrolling: CGFloat = scrollView.contentOffset.y + scrollView.frame.size.height + 1
        if endScrolling >= scrollView.contentSize.height && viewModel.page <= viewModel.totalPage {
            if !viewModel.isApiCalled {
                viewModel.isApiCalled = true
                getAllMovies(true)
            }
        }
    }
}

extension MovieListVC: UISearchBarDelegate {
    
    // This method updates filteredData based on the text in the Search Box
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        viewModel.filterMovies = searchText.isEmpty ? viewModel.movies : viewModel.movies.filter { movies in
            let allWord = movies.title?.split(separator: " ")
            let filterResult = allWord?.filter({ $0.starts(with: searchText) })
            return filterResult?.count != 0
        }
        tableView.reloadData()
    }
}
