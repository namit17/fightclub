//
//  MovieListVM.swift
//  FightClub
//
//  Created by Namit Agrawal on 13/07/21.
//

import Foundation

class MovieListVM {
    
    // data modal
    var filterMovies: [Movies] = []
    var movies: [Movies] = []
    var page: Int = 1
    var totalPage: Int = 1
    var isApiCalled: Bool = false

    func reset() {
        page = 1
        totalPage = 1
        isApiCalled = false
        movies = []
    }
    
    // MARK: - Api Calling Method
    
    func getMoviesAPI(searchText: String, completion: @escaping ((ResponseType, String) -> Void)) {
        let paramters: Parameters = ["api_key": "c2cfb1ea7574392aa22b9fedcbc037ac",
                                     "language": "en-US",
                                     "page": page]
        
        Manager.requestWithParameter(model: MoviesData.self, endPoint: .movies(parameter: paramters), completion: { result in
            switch result {
            case .success(let data):
                self.movies.append(contentsOf: data.results ?? [])
                self.filterMovies = searchText.isEmpty ? self.movies : self.movies.filter { movies in
                    let allWord = movies.title?.split(separator: " ")
                    let filterResult = allWord?.filter({ $0.starts(with: searchText) })
                    return filterResult?.count != 0
                }
                self.totalPage = data.totalPages ?? 0
                self.page += 1
                completion(.success, "")
                
            case .failure(let error):
                var responseType: ResponseType = .failure
                if let error = error as? NetworkError {
                    switch error {
                    case .noInternetConnection:
                        responseType = .noConnection
                                            
                    default:
                        responseType = .failure
                    }
                }
                completion(responseType, error.localizedDescription)
            }
        })
    }
}
